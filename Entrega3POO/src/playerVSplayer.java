public class playerVSplayer extends juego implements Ijuego{

    public void partida(jugador J1, jugador J2, tablero T1){

        char Empty = ' ';

        int columna;
        int cambio = 0;
        boolean insertado = false;


        T1.RellenarTablero(T1.tablero, Empty);
        System.out.println("--- CONNECT 4 ---");
        T1.ImprimirTablero(T1.tablero);

        while(!Final(T1.tablero, Empty) && finJuego==false){
            cambioTurno(cambio);
            if(cambio == 0){
                columna = J1.FichaValidaJ();
                insertado = J1.InsertatrFichaJ(T1.tablero, J1.ficha, Empty, columna);
                if(insertado == true){
                    cambio = 1;
                }
                ComprobarGanador(J1.ficha, T1.tablero);
            }
            else{
                columna = J2.FichaValidaJ();
                insertado = J2.InsertatrFichaJ(T1.tablero, J2.ficha, Empty, columna);
                if(insertado == true){
                    cambio = 0;
                }
                ComprobarGanador(J2.ficha, T1.tablero);

            }
            T1.ImprimirTablero(T1.tablero);
        }
        System.out.println("End of GAME");

    }

}
