import java.util.Scanner;

public class juego implements Ijuego{

    final int filas = 7;
    final int columnas = 8;
    public boolean finJuego = false;

    public void partida(jugador J1, jugador J2, tablero T1){}

    public void cambioTurno(int cambio){
        if(cambio == 0){
            System.out.println("Turn: RED");
        }
        else{
            System.out.println("Turn: YELLOW");
        }
    }

    public boolean Final (char[][] tablero, char vacio){
        for (int i=0; i< filas-1; i++){
            for (int j=0; j< columnas-1; j++){
                if(tablero[i][j] == vacio) {
                    return false;
                }
            }
        }
        return true;
    }

    public void ComprobarGanador(char ficha, char[][] tablero){
        //verificacion ganador (horizontal)
        for (int i = 0; i < filas; i ++) {
            for (int j = 0; j < columnas; j ++) {
                if (tablero[i][j]==ficha && tablero[i][j+1]==ficha && tablero[i][j + 2]==ficha && tablero[i][j+3]==ficha) {
                    finJuego = true;
                    if(ficha == 'R'){
                        System.out.println("Player RED won!!!");
                    }
                    else{
                        System.out.println("Player YELLOW won!!!");
                    }
                }
            }
        }

        //verificacion ganador (vertical)
        for (int i = 0; i < filas; i ++) {
            for (int j = 0; j < columnas; j ++) {
                if (tablero[i][j]==ficha && tablero[i+1][j]==ficha && tablero[i+2][j]==ficha && tablero[i+3][j]==ficha) {
                    finJuego = true;
                    if(ficha == 'R'){
                        System.out.println("Player RED won!!!");
                    }
                    else{
                        System.out.println("Player YELLOW won!!!");
                    }
                }
            }
        }

        //verificacion ganador (diagonal)
        for (int i = 0; i < columnas - 4 + 1; i += 1) {
            for (int j = 0; j < filas - 4 + 1; j += 1) {
                if (tablero[j][i]==ficha && tablero[j + 1][i + 1]==ficha && tablero[j + 2][i + 2]==ficha && tablero[j + 3][i + 3]==ficha) {
                    finJuego = true;
                    if(ficha == 'R'){
                        System.out.println("Player RED won!!!");
                    }
                    else{
                        System.out.println("Player YELLOW won!!!");
                    }
                }
            }
        }
        for (int i = columnas; i > 3; i -= 1) {
            for (int j = 0; j < filas - 3; j += 1) {
                if (tablero[j][i - 1]==ficha && tablero[j + 1][i - 2]==ficha && tablero[j + 2][i - 3]==ficha && tablero[j + 3][i - 4]==ficha) {
                    finJuego = true;
                    if(ficha == 'R'){
                        System.out.println("Player RED won!!!");
                    }
                    else{
                        System.out.println("Player YELLOW won!!!");
                    }
                }
            }
        }

    }

}