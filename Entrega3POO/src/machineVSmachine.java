public class machineVSmachine extends juego implements Ijuego{

    public void partida(maquina M1, maquina M2, tablero T1){

        char Empty = ' ';

        int cambio = 0;
        boolean insertado = false;


        T1.RellenarTablero(T1.tablero, Empty);
        System.out.println("--- CONNECT 4 ---");
        T1.ImprimirTablero(T1.tablero);

        while(!Final(T1.tablero, Empty) && finJuego==false){
            cambioTurno(cambio);
            if(cambio == 0){
                insertado = M1.InsertatrFichaM(T1.tablero, M1.ficha, Empty, M1.columanRandom());
                if(insertado == true){
                    cambio = 1;
                }
                ComprobarGanador(M1.ficha, T1.tablero);
            }
            else{
                insertado = M2.InsertatrFichaM(T1.tablero, M2.ficha, Empty, M2.columanRandom());
                if(insertado == true){
                    cambio = 0;
                }
                ComprobarGanador(M2.ficha, T1.tablero);

            }
            T1.ImprimirTablero(T1.tablero);
        }
        System.out.println("End of GAME");

    }

}
