public class tablero {

    public final int filas = 7;
    final int columnas = 8;
    public char tablero[][];

    public tablero(int filas, int columnas){
         tablero = new char[filas][columnas];
    }

    public void RellenarTablero (char[][] tablero, char ficha){
        for (int i=0; i< filas-1; i++){
            for (int j=0; j< columnas-1; j++){
                tablero[i][j] = ficha;
            }
        }
    }

    public void ImprimirTablero (char[][] tablero){
        System.out.println("----------------------------");
        for (int i=0; i< filas-1; i++){
            for (int j=0; j< columnas-1; j++){
                System.out.print(" | " +tablero[i][j]);
            }
            System.out.println(" |");
        }
        System.out.println("----------------------------");
    }

}
