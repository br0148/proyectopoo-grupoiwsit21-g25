import java.util.Scanner;

public class main {
    static char Red = 'R';
    static char Yellow = 'Y';
    static Scanner telcado = new Scanner(System.in);

    public static void main (String[] args) {

        int opcion;
        System.out.println("Elige el tipoo de partida que quieres jugar: ");
        System.out.println("1- Player VS Player");
        System.out.println("2- Player VS Machine");
        System.out.println("3- Machine VS Machine");
        opcion = telcado.nextInt();

        switch (opcion) {
            case 1:
                //partida entre dos jugadores
                jugador J1 = new jugador(Red);
                jugador J2 = new jugador(Yellow);
                tablero T1 = new tablero(7, 8);
                playerVSplayer juego1 = new playerVSplayer();
                juego1.partida(J1, J2, T1);
                break;
            case 2:
                //partida entre maquina y jugador
                jugador J3 = new jugador(Red);
                maquina M1 = new maquina(Yellow);
                tablero T2 = new tablero(7, 8);
                playerVSmachine juego2 = new playerVSmachine();
                juego2.partida(J3, M1, T2);
                break;
            case 3:
                //partida entre dos maquinas
                maquina M2 = new maquina(Red);
                maquina M3 = new maquina(Yellow);
                tablero T3 = new tablero(7, 8);
                machineVSmachine juego3 = new machineVSmachine();
                juego3.partida(M2, M3, T3);
                break;
            default:
                System.out.println("Error, la seleciion es incorrecta");

        }
    }
}